using BepInEx;
using BepInEx.Configuration;
using HarmonyLib;
using System;

namespace IssySoil
{
	[BepInPlugin("issy.soil", "IssySoil", "0.1"), BepInProcess("DSPGAME.exe")]
	public class IssySoil : BaseUnityPlugin
	{
		private static ConfigEntry<int> threshold;
		private static ConfigEntry<int> ratio;
		private static ConfigEntry<bool> must_hold;

		private const int MAX_SAND_COUNT = 1000000000; // Player.MAX_SAND_COUNT doesn't exist?

		public void Start()
		{
			Harmony.CreateAndPatchAll(typeof(IssySoil));

			threshold = Config.Bind("General", "threshold", 30000, new ConfigDescription("Conversion threshold", new AcceptableValueRange<int>(10000, MAX_SAND_COUNT)));
			ratio = Config.Bind("General", "ratio", 500, new ConfigDescription("Conversion ratio (1 foundation:x soil)", new AcceptableValueRange<int>(1, 10000)));
			must_hold = Config.Bind("General", "must_hold", false, "Only when holding foundations");
		}

		[HarmonyPostfix, HarmonyPatch(typeof(Player), "SetSandCount")]
		public static void Player_SetSandCount_Postfix(Player __instance)
		{
			if (threshold.Value > MAX_SAND_COUNT)
				threshold.Value = MAX_SAND_COUNT;
			if (must_hold.Value && __instance.inhandItemId != 1131)
				return;

			int total = __instance.package.GetItemCount(1131);
			int demand = Math.Max(0, threshold.Value - __instance.sandCount);
			int convert = Math.Min(total, (int) Math.Ceiling((double) demand / ratio.Value));

			if (convert > 0)
			{
				__instance.package.TakeItem(1131, convert, out int inc);
				__instance.SetSandCount(__instance.sandCount + convert * ratio.Value);
			}
		}
	}
}
